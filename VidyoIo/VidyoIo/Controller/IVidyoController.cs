﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VidyoIo
{
    public interface IVidyoController
    {
        //
        // VidyoController.cs needs to be added to both VidyoIo.Android and VidyoIo.iOS. It should NOT be in VidyoIo project.
        //

        // App lifecycle events
        string OnAppStart();
        void OnAppSleep();
        void OnAppResume();

        // Provide the native view
        void SetNativeView(NativeView videoView);
        VidyoConnectorState ConnectorState { get; set; }

        // Events triggered by button clicks from UI
        bool Connect(string host, string token, string displayName, string resourceId);
        void Disconnect();
        void SetMicrophonePrivacy(bool privacy);
        void SetCameraPrivacy(bool privacy);
        void CycleCamera();

        // Orientation has changed
        void OnOrientationChange();
    }
}
