﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace VidyoIo
{
    public class NativeView:View
    {

        public IntPtr Handle { get; set; }
        public float Density { get; set; }

        public uint NativeWidth { get { return (uint)(Width * Density); } }
        public uint NativeHeight { get { return (uint)(Height * Density); } }

        public NativeView() { Density = 1.0F; }
    }
}
