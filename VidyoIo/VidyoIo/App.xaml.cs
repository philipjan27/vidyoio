using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace VidyoIo
{
	public partial class App : Application
	{
        IVidyoController _controller = null;

		public App ()
		{
			InitializeComponent();
            MainPage = new MainPage();
		}

        public App(IVidyoController controller)
        {
            InitializeComponent();
            MainPage mp = new MainPage();
            mp.Initialize(controller);
            MainPage = mp;
            _controller = controller;

        }
       

		protected override void OnStart ()
		{
            // Handle when your app starts
            _controller.OnAppStart();
		}

		protected override void OnSleep ()
		{
            // Handle when your app sleeps
            _controller.OnAppSleep();
		}

		protected override void OnResume ()
		{
            // Handle when your app resumes
            _controller.OnAppResume();
		}
	}
}
