﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Windows.Input;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace VidyoIo
{
    public class ViewModel : INotifyPropertyChanged
    {
        // singleton
        private static IVidyoController vidyoCtrlinstance = null;
        private static ViewModel instance = new ViewModel();
        //public static ViewModel GetInstance();
        public static ViewModel GetInstance(IVidyoController ctrl)     
        {
            vidyoCtrlinstance = ctrl;
            return instance;
        }
      
        public ViewModel()
        {
        }

        const string _defaultIndicatorColor = "#76ff03";
        const string _cameraOnImage = "camera_on.png";
        const string _cameraOffImage = "camera_off.png";
        const string _microphoneOnImage = "microphone_on.png";
        const string _microphoneOffImage = "microphone_off.png";
        const string _callStartImage = "call_start.png";
        const string _callEndImage = "call_end.png";
        string _cameraPrivacyImage = _cameraOnImage;
        string _microphonePrivacyImage = _microphoneOnImage;
        string _callImage = _callStartImage;
        bool _cameraPrivacy = false;
        bool _microphonePrivacy = false;
        bool _buttonConnectEnabled = true;
        bool _allowReconnect = true;
        string _host = "prod.vidyo.io";
        string _token = "cHJvdmlzaW9uAHBoaWxpcGphbmJhcnVpczFAMjNlNTNhLnZpZHlvLmlvADYzNjk2ODMzNzc4AAAzOTUxODgzYmU0MTUxZTI5NDNjZTAxNTA4OTRlOTE5MDg5NDEzYWI5OGE5NjE5YjdhZjY0ZDliZjg0NDkxMGNkMzBjMTM0YjZmYTVhZmMyZDY4NTkyMmFmMGI2NWViNzg="; // INSERT VALID TOKEN
        string _displayName = "philipjanbaruis2";
        string _resourceId = "sampleroom";
        string _toolbarStatus = "Ready to Connect";
        string _clientVersion = "v 0.0.00.x";
        string _hexColor = _defaultIndicatorColor;
        VidyoCallAction _callAction = VidyoCallAction.VidyoCallActionConnect;

        public bool ToggleCameraPrivacy()
        {
            _cameraPrivacy = !_cameraPrivacy;
            CameraPrivacyImage = _cameraPrivacy ? _cameraOffImage : _cameraOnImage;
            return _cameraPrivacy;
        }

        public bool ToggleMicrophonePrivacy()
        {
            _microphonePrivacy = !_microphonePrivacy;
            MicrophonePrivacyImage = _microphonePrivacy ? _microphoneOffImage : _microphoneOnImage;
            return _microphonePrivacy;
        }

        public VidyoCallAction ToggleCallAction()
        {
            CallAction = _callAction == VidyoCallAction.VidyoCallActionConnect ?
                          VidyoCallAction.VidyoCallActionDisconnect : VidyoCallAction.VidyoCallActionConnect;
            return _callAction;
        }

        public VidyoCallAction CallAction
        {
            set
            {
                _callAction = value;
                CallImage = _callAction == VidyoCallAction.VidyoCallActionConnect ? _callStartImage : _callEndImage;
            }
            get { return _callAction; }
        }

        public string Host
        {
            get { return _host; }
            set
            {
                if (_host != value)
                {
                    _host = value;
                    OnPropertyChanged("Host");
                }
            }
        }

        public string Token
        {
            get { return _token; }
            set
            {
                if (_token != value)
                {
                    _token = value;
                    OnPropertyChanged("Token");
                }
            }
        }

        public string DisplayName
        {
            get { return _displayName; }
            set
            {
                if (_displayName != value)
                {
                    _displayName = value;
                    OnPropertyChanged("DisplayName");
                }
            }
        }

        public string ResourceId
        {
            get { return _resourceId; }
            set
            {
                if (_resourceId != value)
                {
                    _resourceId = value;
                    OnPropertyChanged("ResourceId");
                }
            }
        }

        public string ToolbarStatus
        {
            get { return _toolbarStatus; }
            set
            {
                if (_toolbarStatus != value)
                {
                    _toolbarStatus = value;
                    OnPropertyChanged("ToolbarStatus");
                }
            }
        }

        public string ClientVersion
        {
            get { return _clientVersion; }
            set
            {
                if (_clientVersion != value)
                {
                    _clientVersion = value;
                    OnPropertyChanged("ClientVersion");
                }
            }
        }

        public string CallImage
        {
            get { return _callImage; }
            set
            {
                if (_callImage != value)
                {
                    _callImage = value;
                    OnPropertyChanged("CallImage");
                }
            }
        }

        public string CameraPrivacyImage
        {
            get { return _cameraPrivacyImage; }
            set
            {
                if (_cameraPrivacyImage != value)
                {
                    _cameraPrivacyImage = value;
                    OnPropertyChanged("CameraPrivacyImage");
                }
            }
        }

        public string MicrophonePrivacyImage
        {
            get { return _microphonePrivacyImage; }
            set
            {
                if (_microphonePrivacyImage != value)
                {
                    _microphonePrivacyImage = value;
                    OnPropertyChanged("MicrophonePrivacyImage");
                }
            }
        }

        public bool ButtonConnectEnabled
        {
            get { return _buttonConnectEnabled; }
            set
            {
                if(_buttonConnectEnabled != value)
                {
                    _buttonConnectEnabled = value;
                    OnPropertyChanged("ButtonConnectEnabled");
                }
            }
        }

        public string ColorIndicator
        {
            get { return _hexColor; }
            set
            {
                if(_hexColor != value)
                {
                    _hexColor = value;
                    OnPropertyChanged("ColorIndicator");
                }
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var changed = PropertyChanged;
            if (changed != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        //
        //  Button click listeners here!
        //
        private ICommand _vidyoConnectCommand;
        public ICommand VidyoConnectCommand
        {
            get { return _vidyoConnectCommand = _vidyoConnectCommand ?? new DelegateCommand(async (obj) => await VidyoConnectEvent()); }
        }

        private async Task VidyoConnectEvent()
        {
            if(CallAction == VidyoCallAction.VidyoCallActionConnect)
            {
                ToolbarStatus = "Connecting...";

                if (!vidyoCtrlinstance.Connect(Host, Token, DisplayName, ResourceId))
                {
                    // failed to connect!
                    state = VidyoConnectorState.VidyoConnectorStateConnectionFailure;
                    ColorIndicator = _defaultIndicatorColor;
                }
                else
                {
                    // connected!
                    CallAction = VidyoCallAction.VidyoCallActionDisconnect;
                    ColorIndicator = "#40c4ff";
                }

            }
            else
            {
                ToolbarStatus = "Disconnecting...";
                vidyoCtrlinstance.Disconnect();
            }
        }

        //
        // change camera command
        //
        private ICommand _vidyoCameraChangedCommand;
        public ICommand VidyoCameraChangeCommand
        {
            get { return _vidyoCameraChangedCommand = _vidyoCameraChangedCommand ?? new DelegateCommand(async (obj) => VidyoCameraChangeEvent()); }
            
        }

        public async Task VidyoCameraChangeEvent()
        {
             vidyoCtrlinstance.CycleCamera();
        }

        //
        // hide/show camera command
        //
        private ICommand _vidyoCameraEnableDisableCommand;
        public ICommand VidyoCameraEnableDisableCommand
        {
            get { return _vidyoCameraEnableDisableCommand = _vidyoCameraEnableDisableCommand ?? new DelegateCommand(async (obj) => VidyoCameraEnableDisableEvent()); }
        }

        public async Task VidyoCameraEnableDisableEvent()
        {
            vidyoCtrlinstance.SetCameraPrivacy(ToggleCameraPrivacy());
        }


        //
        // enable/disable microphone
        //
        private ICommand _vidyoAudioEnableOrDisableCommand;
        public ICommand VidyoAudioEnableOrDisableCommand
        {
            get { return _vidyoAudioEnableOrDisableCommand = _vidyoAudioEnableOrDisableCommand ?? new DelegateCommand(async (obj) => VidyoAudioEnableOrDisableEvent()); }
        }
        public async Task VidyoAudioEnableOrDisableEvent()
        {
            vidyoCtrlinstance.SetMicrophonePrivacy(ToggleMicrophonePrivacy());
        }


        // get the current connection state - Connected or Disconnected
        public VidyoConnectorState state
        {
            set
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    //check current status
                    _callAction = value == VidyoConnectorState.VidyoConnectorStateConnected ? VidyoCallAction.VidyoCallActionDisconnect : VidyoCallAction.VidyoCallActionConnect;

                    //get current status to update Label Indicator
                    ToolbarStatus = VidyoDef.StateDescription[value];

                    if (VidyoConnectorState.VidyoConnectorStateConnected == value)
                    {
                        // do something when its connected
                    }
                    else
                    {
                        if (!_allowReconnect && (value == VidyoConnectorState.VidyoConnectorStateDisconnected))
                        {
                            ButtonConnectEnabled = false;
                            ToolbarStatus = "Call Ended";
                        }
                    }
                });
            }
        }
    }
}
