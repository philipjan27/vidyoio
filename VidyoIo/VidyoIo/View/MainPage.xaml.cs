﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.ComponentModel;

namespace VidyoIo
{
	public partial class MainPage : ContentPage
	{
        IVidyoController _vidyoController = null;
        ViewModel _viewModel = null;
        double _pageWidth = 0;
        double _pageHeight = 0;

        public MainPage()
		{
			InitializeComponent();          
        }

        public void Initialize(IVidyoController vidyoController)
        {
            _viewModel = ViewModel.GetInstance(vidyoController);
            BindingContext = _viewModel;
            vidyoController.SetNativeView(videoView);
            _vidyoController = vidyoController;

            INotifyPropertyChanged i = (INotifyPropertyChanged)_vidyoController;
            i.PropertyChanged += new PropertyChangedEventHandler(VidyoControllerPropertyChanged);
        }

        void VidyoControllerPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ConnectorState")
            {
                _viewModel.state = _vidyoController.ConnectorState;

                // set the current Callaction base on VidyoController state (for updating button image source)
                _viewModel.CallAction = _vidyoController.ConnectorState == VidyoConnectorState.VidyoConnectorStateConnected ? VidyoCallAction.VidyoCallActionDisconnect : VidyoCallAction.VidyoCallActionConnect;
            }
        }


        // For some reason, when OnSizeAllocated method is contained, the UI preview in MainPage.xaml.cs is not shown.
        // Set #if to false if you want to see a valid preview of the UI.
#if true
        // Handle orientation change
        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (Math.Abs(width - _pageWidth) > 0.001 || Math.Abs(height - _pageHeight) > 0.001)
            {
                _pageWidth = width;
                _pageHeight = height;
                _vidyoController.OnOrientationChange();
            }
        }
#endif
    }
}
