﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using VidyoIo;
using VidyoIo.Droid;
using VidyoClient;


[assembly: Xamarin.Forms.Dependency(typeof(VidyoInitService_Android))]
namespace VidyoIo.Droid
{
    public class VidyoInitService_Android : IVidyoInitService
    {
        public bool Initialize()
        {
            var activity = (Activity)Forms.Context;
            ConnectorPKG.SetApplicationUIContext(activity);
            bool result=ConnectorPKG.Initialize();
            return result;
        }
    }
}