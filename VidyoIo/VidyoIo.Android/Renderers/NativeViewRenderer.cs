﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VidyoIo;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Android.Util;
using Android.Content;

[assembly: Xamarin.Forms.ExportRenderer(typeof(VidyoIo.NativeView),typeof(VidyoIo.Droid.NativeViewRenderer))]
namespace VidyoIo.Droid
{
    public class NativeViewRenderer : Xamarin.Forms.Platform.Android.ViewRenderer<VidyoIo.NativeView,FrameLayout>
    {
        FrameLayout frameLayout;
        public NativeViewRenderer()
        { }

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<VidyoIo.NativeView> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                frameLayout = new FrameLayout(this.Context);
                SetNativeControl(frameLayout);
            }

            if(e.OldElement != null)
            {
                VidyoController.GetInstance().Cleanup();
            }

            if(e.NewElement != null)
            {
#if true
                IWindowManager manager = Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
                var displayMetrics = new DisplayMetrics();
                manager.DefaultDisplay.GetMetrics(displayMetrics);
                e.NewElement.Density = displayMetrics.Density;
#endif
                e.NewElement.Handle = this.Control.Handle;
            }
        }
    }
}